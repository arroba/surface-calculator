package com.crxmarkets.babich.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Collections;
import java.util.List;

/**
 * It is a data transfer object that is used for REST endpoints and contains surface descriptions.
 * @author Vadim Babich
 */
public class Surface {

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private final List<Integer> profile;

    @JsonCreator
    public Surface(@JsonProperty("profile") List<Integer> profile) {
        this.profile = profile;
    }

    public List<Integer> getProfile() {
        return null == profile ? Collections.emptyList() : Collections.unmodifiableList(profile);
    }

    @Override
    public String toString() {
        return "Surface{" +
                "profile=" + profile +
                '}';
    }
}

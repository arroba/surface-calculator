package com.crxmarkets.babich.rest.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Error is appeared when surface calculation generate following problems as null input surface in argument or max integer value overload.
 * @author Vadim Babich
 */
@ResponseStatus(HttpStatus.BAD_REQUEST)
public class SurfaceCalculationError extends RuntimeException{

    public SurfaceCalculationError(String message) {
        super(message);
    }
}

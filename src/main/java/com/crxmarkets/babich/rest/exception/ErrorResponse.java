package com.crxmarkets.babich.rest.exception;

import java.util.StringJoiner;

/**
 * This is value object that used to transfer error to client
 * @author Vadim Babich
 */
public class ErrorResponse {

    private final String message;

    public ErrorResponse(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", ErrorResponse.class.getSimpleName() + "[", "]")
                .add("message='" + message + "'")
                .toString();
    }
}

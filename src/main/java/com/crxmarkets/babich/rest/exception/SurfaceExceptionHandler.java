package com.crxmarkets.babich.rest.exception;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

/**
 * Controller error interceptor, that prevents internal errors reaching the client side.
 * @author Vadim Babich
 */
@ControllerAdvice
public class SurfaceExceptionHandler extends ResponseEntityExceptionHandler {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @ExceptionHandler(SurfaceCalculationError.class)
    public ResponseEntity<Object> handleUserNotFoundException(SurfaceCalculationError ex, WebRequest request) {
        return new ResponseEntity<>(new ErrorResponse("Error when calculation surface: " + ex.getLocalizedMessage())
                , HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<Object> handleAllExceptions(Exception ex, WebRequest request) {
        logger.error("An issue has occurred, {}", ex.getMessage(), ex);
        return new ResponseEntity<>(new ErrorResponse("Server error"), HttpStatus.INTERNAL_SERVER_ERROR);
    }

}

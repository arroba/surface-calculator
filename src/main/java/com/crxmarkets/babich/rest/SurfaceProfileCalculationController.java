package com.crxmarkets.babich.rest;

import com.crxmarkets.babich.dto.Surface;
import com.crxmarkets.babich.service.SurfaceProfileCalculator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

/**
 * This is the REST endpoint controller for resource of surface.
 * @author Vadim Babich
 */
@Controller
@RequestMapping("/surface")
public class SurfaceProfileCalculationController {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private SurfaceProfileCalculator service;

    /**
     * endpoint to calculates the volume of water which remained after the rain, in units.
     * @param surfaceProfile object that describes profile of a surface.
     * @return volume units of water.
     */
    @PostMapping("/water")
    public @ResponseBody
    int calculateWaterVolume(@RequestBody Surface surfaceProfile) {

        logger.debug("Calculation surface profile {}", surfaceProfile);

        List<Integer> profile = surfaceProfile.getProfile();

        int[] surface = Optional.ofNullable(profile)
                .orElse(Collections.emptyList()).stream()
                .mapToInt(Integer::intValue).toArray();

        return service.calculateWaterVolumeFor(surface);
    }


}

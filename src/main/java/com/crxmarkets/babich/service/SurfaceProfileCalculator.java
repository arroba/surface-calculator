package com.crxmarkets.babich.service;

import com.crxmarkets.babich.rest.exception.SurfaceCalculationError;
import org.springframework.stereotype.Service;

import java.util.ArrayDeque;
import java.util.Deque;

/**
 * Calculates the volume of water which remained after the rain, in units.
 * @author Vadim Babich
 */
@Service
public class SurfaceProfileCalculator {

    static class Altitude {
        private final int value;
        private final int size;

        public Altitude(int value, int size) {
            this.value = value;
            this.size = size;
        }

        public int getValue() {
            return value;
        }

        public int getSize() {
            return size;
        }

        @Override
        public String toString() {
            return "Altitude{" +
                    "value=" + value +
                    ", size=" + size +
                    '}';
        }
    }

    /**
     * Calculate the volume units of water for surface profiles
     * </p>{@code IllegalArgumentException} if argument {@code surfaceProfile} is null or volume of water
     * is bigger than max integer value.
     * @param surfaceProfile array of integer numbers describes profile of a surface.
     * @return volume units of water.
     */
    public int calculateWaterVolumeFor(int[] surfaceProfile) {
        if (null == surfaceProfile) {
            throw new SurfaceCalculationError("'surfaceProfile' cannot be null");
        }

        final Deque<Altitude> stack = new ArrayDeque<>();

        int waterVolume = 0;
        for (int value : surfaceProfile) {
            Altitude previousHill = stack.peek();
            //the first step
            if (null == previousHill) {
                stack.offerFirst(new Altitude(value, 1));
                continue;
            }

            //going down the stairs
            if (previousHill.getValue() >= value) {
                stack.offerFirst(new Altitude(value, 1));
                continue;
            }

            validateVolume(waterVolume += calculateValleyWaterVolume(value, stack));
        }

        return waterVolume;
    }

    private int calculateValleyWaterVolume(int rightHillHeight, Deque<Altitude> stack) {
        int waterVolume = 0;
        int leftHillHeight = 0;
        int valleySize = 0;

        while (true) {
            Altitude altitude = stack.peek();
            //came to the left side of the valley
            if (null == altitude) {
                //if the left hill is lower than the right one, we must apply a difference correction
                if (leftHillHeight < rightHillHeight) {
                    waterVolume -= (rightHillHeight - leftHillHeight) * valleySize;
                }
                break;
            }

            leftHillHeight = altitude.getValue();

            if (leftHillHeight >= rightHillHeight) {
                break;
            }

            valleySize += altitude.getSize();
            int depth = validateVolume(rightHillHeight - leftHillHeight);
            validateVolume(waterVolume += altitude.getSize() * depth);
            stack.removeFirst();
        }

        stack.offerFirst(new Altitude(rightHillHeight, valleySize + 1));

        return waterVolume;
    }

    private static int validateVolume(int value){
        if(0 > value){
            throw new SurfaceCalculationError("Volume of water is bigger than max integer value.");
        }
        return value;
    }
}

package com.crxmarkets.babich;

import com.crxmarkets.babich.dto.Surface;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.converter.ArgumentConversionException;
import org.junit.jupiter.params.converter.ConvertWith;
import org.junit.jupiter.params.converter.SimpleArgumentConverter;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@RunWith(SpringRunner.class)
class ApplicationIT {

    @Autowired
    private MockMvc underTestMockMvc;

    @Autowired
    private ObjectMapper objectMapper;


    @DisplayName("Should be Ok code returned from the '/surface/water' endpoint.")
    @ParameterizedTest(name = "expecting {1} for calculation on surface {0}.")
    @CsvSource({
            "',', 0",
            "'3, 2, 4, 1, 2', 2",
            "'4, 1, 1, 0, 2, 3', 8"
    })
    void givenSurface_calculateWaterVolume_thenOkCode(
            @ConvertWith(ArrayAsStringToSurface.class) Surface surface, int expectedValue) throws Exception {
        String surfaceAsJson = objectMapper.writeValueAsString(surface);

        MvcResult result = underTestMockMvc.perform(post("/surface/water")
                .content(surfaceAsJson)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))

                .andExpect(status().isOk())
                .andReturn();

        Integer waterVolume = objectMapper
                .readerFor(Integer.class)
                .readValue(result.getResponse().getContentAsString());

        Assertions.assertEquals(expectedValue, waterVolume);
    }


    @DisplayName("Should be BadRequestCode returned from the '/surface/water' endpoint when illegal surface value.")
    @ParameterizedTest(name = "expecting {1} for calculation on surface {0}.")
    @CsvSource({
            "'1, 2, 2147483647, -1147483648, 2147483647, 20000, 1, 20000', com.crxmarkets.babich.rest.exception.SurfaceCalculationError"
    })
    void givenIllegalValueSurface_calculateWaterVolume_thenBadRequestCode(
            @ConvertWith(ArrayAsStringToSurface.class) Surface surface, Class<?> exception) throws Exception {
        String surfaceAsJson = objectMapper.writeValueAsString(surface);

        underTestMockMvc.perform(post("/surface/water")
                .content(surfaceAsJson)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))

                .andExpect(status().isBadRequest())
                .andExpect(mvcResult -> assertEquals(mvcResult.getResolvedException().getClass(), exception));
    }

    private static class ArrayAsStringToSurface extends SimpleArgumentConverter {

        @Override
        protected Object convert(Object o, Class<?> aClass) throws ArgumentConversionException {
            assertEquals(Surface.class, aClass, "Can only convert to Surface");
            return new Surface(Pattern.compile("[^\\d-]+").splitAsStream((CharSequence) o)
                    .map(Integer::valueOf)
                    .collect(Collectors.toList()));
        }
    }
}
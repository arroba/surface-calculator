package com.crxmarkets.babich.service;

import com.crxmarkets.babich.rest.exception.SurfaceCalculationError;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.converter.ArgumentConversionException;
import org.junit.jupiter.params.converter.ConvertWith;
import org.junit.jupiter.params.converter.SimpleArgumentConverter;
import org.junit.jupiter.params.provider.CsvSource;

import java.util.regex.Pattern;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class SurfaceProfileCalculatorTest {

    private SurfaceProfileCalculator surfaceProfileCalculator = new SurfaceProfileCalculator();


    @DisplayName("Should be calculate the volume of water for surface profiles")
    @ParameterizedTest(name = "Surface {0} should have water volume {1}.")
    @CsvSource({
            "',', 0",
            "'0,', 0",
            "'0, 0', 0",
            "'1, 0', 0",
            "'0, 1', 0",
            "'1, 1', 0",
            "'1, 1, 1', 0",
            "'1, 0, 0, 0, 1', 3",
            "'3, 2, 4, 1, 2', 2",
            "'4, 1, 1, 0, 2, 3', 8",
            "'1, 2, 3, 0, 3, 2, 1', 3",
            "'1, 2, 3, -4, 3, 2, 1', 7",
    })
    void ShouldBeCalculateVolumeOfWaterForSurfaceProfiles_calculateWaterVolumeFor(
            @ConvertWith(SurfaceProfileCalculatorTest.StringToIntArray.class) int[] surface, int expectedVolume) {

        assertEquals(expectedVolume, surfaceProfileCalculator.calculateWaterVolumeFor(surface));
    }


    @DisplayName("Should be crashed due to maximum integer value overload")
    @ParameterizedTest(name = "expecting SurfaceCalculationError for calculation for surface {0}.")
    @CsvSource({
            "'1, 2, 2147483647, -1147483648, 2147483647, 20000, 1, 20000'"
    })
    void ShouldBeCrashedDueToMaximumIntegerValueOverload_calculateWaterVolumeFor(
            @ConvertWith(SurfaceProfileCalculatorTest.StringToIntArray.class) int[] surface) {

        assertThrows(SurfaceCalculationError.class, () -> surfaceProfileCalculator.calculateWaterVolumeFor(surface));
    }


    private static class StringToIntArray extends SimpleArgumentConverter {

        @Override
        protected Object convert(Object o, Class<?> aClass) throws ArgumentConversionException {
            assertEquals(int[].class, aClass, "Can only convert to int[]");
            return Pattern.compile("[^\\d-]+").splitAsStream((CharSequence) o)
                    .mapToInt(Integer::valueOf)
                    .toArray();
        }
    }
}
# CRX Market Java EE Coding Exercise (Spring Boot, REST)

This is a Java Spring Boot application. It has a REST endpoint for calculating the surface profile.

Complexity of your solution is **Q(n)**.

### Agenda

Imagine that the array describes profile of a surface, for example:

![layers](images/pic1.png)

Now imagine that there was a heavy rain, and all possible "holes" are filled with water.
In this case, we have volume == 2 units of water:

![layers](images/pic2.png)

Another example, here we have volume == 8 units of water:

![layers](images/pic3.png)

### Requirements

1. Write an application which takes an array as an input, and calculates the volume of water which remained after the rain, in units.
1. The application shall be deployable in a EJB container of your choice
   (preferably either JBoss, Wildfly, Glassfish, or TomEE).
1. We also accept Spring Boot solutions as an alternative to a JEE conform packaging.
1. Make a statement on complexity of your solution (time and memory).

## Getting Start

This section contains instructions on running localy.

### Building and running Application

1. Clone this repository.
1. Compile the Application.

    `mvn clean install -DskipTests`
    
1. Start the Application.

    `java -jar target/surface-profile-calculator-1.0-SNAPSHO.jar`
    
### Building and running with Docker
1. Clone this repository.
1. Compile and create a Docker image, which has name `${env.USER}/surface-profile-calculator`.
 
     `mvn clean install -DskipTests dockerfile:build`
   
   ```
   % docker images -a
   REPOSITORY                               TAG                      IMAGE ID       CREATED          SIZE
   ${env.USER}/surface-profile-calculator   latest                   05d21a62aa99   16 seconds ago   360MB
   openjdk                                  14-jdk-alpine            8273876b08aa   11 months ago    340MB   
   ```  
1. Start the Application.

   `docker run --rm --detach --publish=8080:8080 --name=calculator ${env.USER}/surface-profile-calculator`
    
    
### REST API

1. Request to calculates the volume of water which remained after the rain, in units:
    ```
      POST http://localhost:8080/surface/water
      Content-Type: application/json

      {
         "profile": [1, 2, 3, -4, 3, 2, 1]
      }
    ```
    response 
    ```
      HTTP/1.1 200
      Content-Type: application/json
      Transfer-Encoding: chunked      
      Keep-Alive: timeout=60
      Connection: keep-alive

      7

      Response code: 200; Time: 482ms; Content length: 1 bytes
    ```
1. Request to getting **error**:
    ```
      POST http://localhost:8080/surface/water
      Content-Type: application/json

      {
         "profile": [1, 2, 2147483647, -1147483648, 2147483647, 20000, 1, 20000]
      }
    ```
   responce
    ```
      HTTP/1.1 400
      Content-Type: application/json
      Transfer-Encoding: chunked
      Date: Tue, 19 Jan 2021 15:45:25 GMT
      Connection: close

      {
         "message": "Error when calculation surface: Volume of water is bigger than max integer value."
      }

      Response code: 400; Time: 312ms; Content length: 95 bytes
```
